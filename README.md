symfony_usersystem
=====

A Symfony project created on August 14, 2017, 4:07 pm.

# Installation
Requirements:
- GIT
- PHP
- Composer

## Installing PHP
### Windows
 - Get PHP7.0.22 (http://windows.php.net/downloads/releases/php-7.0.22-Win32-VC14-x64.zip)
 - Unzip it in C:\php\ , add C:\php to your PATH variable.
 - Create a php.ini file (Copy php.ini-development to php.ini)
 - Enable the extensions hereunder
    - php_intl.dll
    - php_pdo_mysql.dll
    - php_openssl.dll

### Unix
- Get PHP7.0.22
- Make sure it's available on $PATH.

## Getting composer
- Get composer (https://getcomposer.org/download/)
- Make sure the folder where your composer resides is also in your PATH variable.

## Creating the MySQL scheme & Import default data
```sh
$ php bin/console doctrine:schema:create
$ php bin/console doctrine:schema:update --force
$ php bin/console doctrine:fixtures:load
```

## Running the webserver
```sh
$ php bin/console server:run
```
