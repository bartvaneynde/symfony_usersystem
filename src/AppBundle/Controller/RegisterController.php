<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Form\RegisterType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends Controller
{
    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer)
    {
        $User = new User();
        $form = $this->createForm(RegisterType::class, $User);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $User = $form->getData();
            $User->setCreated(new \DateTime());
            $User->setChanged(new \DateTime());
            $User->setIsActive(1);
            $User->setPassword($passwordEncoder->encodePassword($User, $User->getPassword()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($User);
            $em->flush();

            $message = (new \Swift_Message('You registered!'))
                ->setFrom('noreply@bwf.be')
                ->setTo($User->getEmail())
                ->setBody(
                    $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/registration.html.twig',
                        array('name' => $User->getUsername())
                    ),
                    'text/html'
                )
                /*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'Emails/registration.txt.twig',
                        array('name' => $name)
                    ),
                    'text/plain'
                )
                */
            ;

            $mailer->send($message);

            return $this->redirectToRoute('login');
        }

        return $this->render('Register/register.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/register/mail", name="registermail")
     */
    public function registermailAction(Request $request)
    {
        return $this->render('Emails/registration.html.twig', array(
            'name' => 'bruno',
        ));
    }
}
